package testRunners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(
		features = {"C:\\Users\\User\\Eclipse2021-06\\CucumberPractice\\testresources\\appFeatures\\Search.feature"},
		glue = {"stepdefinitions", "AmazonHooks"},
		plugin = {"pretty"}
		)

public class AmazonTest {

}
