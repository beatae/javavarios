package testRunners;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(
		features = {"C:\\Users\\User\\Eclipse2021-06\\CucumberPractice\\testresources\\appFeatures\\Registration.feature"},
		glue = {"stepdefinitions"},
		plugin = {"pretty",
					"json:target\\MyReports\\report.json",
					"junit:target\\MyReports\\report.xml"

		}
		
		)


public class RegSteps {

}
