package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {
	@Given("User is on login page")
	public void user_is_on_login_page() {
	  
	}

	@When("the user clicks on login button")
	public void the_user_clicks_on_login_button() {
	    
	}

	@Then("the login page is displayed")
	public void the_login_page_is_displayed() {
	    
	}

	@When("the user enters the {string} in the user field")
	public void the_user_enters_the_in_the_user_field(String string) {
	    
	}

	@When("the user enters the {string} in the password field")
	public void the_user_enters_the_in_the_password_field(String string) {
	   
	}

	@When("the user clicks on the button login")
	public void the_user_clicks_on_the_button_login() {
	   
	}

	@Then("the user gets the login fail message")
	public void the_user_gets_the_login_fail_message() {
	   
	}


}
