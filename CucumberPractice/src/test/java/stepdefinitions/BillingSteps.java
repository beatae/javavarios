package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class BillingSteps {
	
	double billingAmount;
	double taxAmount;
	double finalAmount;

	@Given("user is in billing page")
	public void user_is_in_billing_page() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new io.cucumber.java.PendingException();
	}

	@When("user enters billing amount {string}")
	public void user_enters_billing_amount(String billingAmount) {
	    // Write code here that turns the phrase above into concrete actions
	    this.billingAmount = Double.parseDouble(billingAmount);
	    
	}

	@When("user enter tax amount {string}")
	public void user_enter_tax_amount(String taxAmount) {
	    this.taxAmount = Double.parseDouble(taxAmount);
	}

	@When("user clicks on calculate button")
	public void user_clicks_on_calculate_button() {
	    
	}

	@Then("the final amount is displayed {string}")
	public void the_final_amount_is_displayed(String expectedFinalAmount) {
		this.finalAmount = this.billingAmount + this.taxAmount;
		System.out.println("expected final amount" + Double.parseDouble(expectedFinalAmount));
		System.out.println("final amount" + this.finalAmount);
		Assert.assertTrue(finalAmount == Double.parseDouble(expectedFinalAmount));
	}

}
