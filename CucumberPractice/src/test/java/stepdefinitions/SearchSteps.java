package stepdefinitions;


import amazonimplementation.Product;
import amazonimplementation.Search;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;

public class SearchSteps {
	
	Product product;
	Search search;

	@Given("I have a search field in Amazon")
	public void i_have_a_search_field_in_amazon() {
	    System.out.println("Step1");
	}

	@When("I search for a product with name {string} and price {int}")
	public void i_search_for_a_product_with_name_and_price(String productName, Integer price) {
		System.out.println("Step 2 + "+ productName);	
		
		product = new Product(productName,price);
	}
	@Then("Product with name {string} should be displayed")
	public void product_with_name_should_be_displayed(String productName) {
		System.out.println("Step 3 + "+ productName);	
		
		search = new Search();
		String name = search.displayProduct(product);
		System.out.println("the product name is " + name);
		
		Assert.assertEquals(product.getProductName(), name);
		
	}
	
	
}
