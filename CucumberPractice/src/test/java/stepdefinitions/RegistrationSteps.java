package stepdefinitions;

import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RegistrationSteps {
	
	@Given("User is on registration page")
	public void user_is_on_registration_page() {
	    
	}

	@When("User enters the following details")
	public void user_enters_the_following_details(DataTable dataTable) {
	   List<List<String>> userString = dataTable.asLists(String.class);
	   for (List<String> e : userString) {
		   System.out.println(e);
	   }
		
	}
	@When("User enters the following details with columns")
	public void user_enters_the_following_details_with_columns(DataTable dataTable) {
	    List<Map<String, String>> userMap = dataTable.asMaps(String.class, String.class);
	   // System.out.println(userMap);
	    //System.out.println(userMap.get(1).get("Name")); // Prints the Name of the 1st array
	    for(Map<String,String> e:userMap) {
	    		System.out.println(e.get("Name"));
	    		System.out.println(e.get("Task"));
	    		System.out.println(e.get("email"));
	    		System.out.println(e.get("id"));
	    		System.out.println(e.get("City"));
	    		
	    }
	}

	@Then("user registration should be  succesfull")
	public void user_registration_should_be_succesfull() {
	    
	}
	
	

}
