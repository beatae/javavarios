package stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UberSteps {

	@Given("User wants to select a car type {string} from uber app")
	public void user_wants_to_select_a_car_type_from_uber_app(String carType) {
	    System.out.println("Step 1" + " "+ carType);
	    int i = 9/0;
	}

	@When("User selects car type {string} and pickup point {string} and drop location {string}")
	public void user_selects_car_type_and_pickup_point_and_drop_location(String carType, String pickLocation, String dropLocation) {
		System.out.println("Step 2" +  " "+ carType + " "+  pickLocation + " "+  dropLocation );
	}

	@Then("User starts the ride")
	public void user_starts_the_ride() {
		System.out.println("Step 3");
	}

	@Then("Driver ends the ride")
	public void driver_ends_the_ride() {
		System.out.println("Step 4");
	}

	@Then("User pays {int} USD")
	public void user_pays_usd(Integer price) {
		System.out.println("Step 5" + " "+  price);
	}
	
}
