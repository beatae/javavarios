package AppHooks;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.Scenario;

public class AmazonHooks {
	
	
	@Before(order = 1)
	public void setupBrowser(Scenario sc) {
		System.out.println("Open browser");
		System.out.println(sc.getName());
	}
	@Before(order = 2)
	public void setupUrl() {
		System.out.println("Open url");
	}

	@After(order = 2)
	public void closeDown() {
		System.out.println("Close browser");
	}
	@After(order = 1)
	public void tearDown(Scenario sc) {
		System.out.println("logout account");
		System.out.println(sc.getName());
	}
	
	@BeforeStep
	public void takeScreenshot() {
		System.out.println("take screenshot");
		
	}
	
	@AfterStep
	public void refreshPage() {
		System.out.println("Refresh the Page");
		
	}
	
}
