Feature: Login feature

Scenario Outline: Login fail - possible combinations
Given User is on login page
When the user clicks on login button
Then the login page is displayed
When the user enters the "<UserName>" in the user field
And  the user enters the "<Password>" in the password field
And the user clicks on the button login
Then the user gets the login fail message

    Examples: 
      | Username  		| Password |
      | incorrectUser |    12364 | 
      | anotherincorr |   233335 | 
      | otheriusdjddd |   252556 | 
