Feature: Uber booking feature

Scenario: Booking cab
Given User wants to select a car type "sedan" from uber app
When User selects car type "sedan" and pickup point "Bangalore" and drop location "pune"
Then User starts the ride
And Driver ends the ride
Then User pays 100 USD
