Feature: Calculate billing amount

Scenario Outline: Billing amount
Given user is in billing page
When user enters billing amount "<billingamount>"
When user enter tax amount "<taxmount>"
And user clicks on calculate button
Then the final amount is displayed "<finalamount>"

    Examples: 
      | billingamount  | taxamount | finalamount  |
      | 1000					 |   10			 | 1010				  |
      | 500						 |   20			 | 520			    |
		  | 100						 |   5.5		 | 105.5			  |