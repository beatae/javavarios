Feature: User registration

Scenario: user registration with different data
Given User is on registration page
When User enters the following details
	|Josh |automation | nav@gmail.com |123654 | Bangalore |
	|will |testing |will@mail.com |326545 | Pune |
	| lisa | dev |liv@gmail.com | 325685 | SFO |
	
Scenario: user registration with different data columns
Given User is on registration page
When User enters the following details with columns
	|Name |Task | mail | id | City|
	|Josh |automation | nav@gmail.com |123654 | Bangalore |
	|will |testing |will@mail.com |326545 | Pune |
	| lisa | dev |liv@gmail.com | 325685 | SFO |
Then user registration should be  succesfull
