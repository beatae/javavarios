package javalessons;

import java.util.Scanner;

public class OpeTernario {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(); 
		
		Scanner scanner = new Scanner(System.in); //  leer en info de la consola
		
		System.out.println("Inserta un numero y presiona enter"); 
		int a = scanner.nextInt();
		System.out.println("Inserta otro numero y presiona enter"); 
		int b = scanner.nextInt();
				
		var resultado =(a>=b) ? "si" : "no"; // simplifica el if else
		
		
		
		System.out.println(resultado); 
	}

}
