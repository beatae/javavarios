package javalessons;

import java.util.Scanner;

public class EjercicioIf2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				//System.out.println(); 			
				//Scanner scanner = new Scanner(System.in); //  leer en info de la consola		
				//System.out.println("Inserta un numero y presiona enter"); 
				//int a = scanner.nextInt();
		
		Scanner scanner = new Scanner(System.in); //  leer en info de la consola		
		System.out.println("Inserta un numero de mes y presiona Enter"); 
		var mes = scanner.nextInt();
		
		
		String  estacion;
		
		if(mes ==1 || mes == 2 || mes == 12) {
			estacion = "Invierno";
			
		}
		else if (mes ==3 || mes == 4 || mes == 5){
			estacion = "primavera";
		}
		else if (mes ==6 || mes == 7 || mes == 8){
			estacion = "verano";
		}
		
		else if (mes ==9 || mes == 10 || mes == 11){
			estacion = "otono";
		}
		else {
			estacion = "mes incorrecto";
		}
		System.out.println("la estacion es "+ estacion + " para el mes " + mes);
		
}
}
