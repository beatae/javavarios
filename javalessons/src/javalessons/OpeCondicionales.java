package javalessons;

import java.util.Scanner;

public class OpeCondicionales {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(); 
		
		
		int valorMin = 0;
		int valorMax = 10;
		
		Scanner scanner = new Scanner(System.in); //  leer en info de la consola
		
		System.out.println("Inserta un numero y presiona enter"); 
		int a = scanner.nextInt();
		
		boolean result = a>= valorMin && a<= valorMax; // verificar si un valor se encuentra entre
		System.out.println(result);
		
	}

}
