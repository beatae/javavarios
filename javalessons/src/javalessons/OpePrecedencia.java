package javalessons;

import java.util.Scanner;

public class OpePrecedencia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(); 			
		//Scanner scanner = new Scanner(System.in); //  leer en info de la consola		
		//System.out.println("Inserta un numero y presiona enter"); 
		//int a = scanner.nextInt();
		
		var x = 5;
		var y =10;
		var z = ++x + y--; //  x 
		
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
	}

}
