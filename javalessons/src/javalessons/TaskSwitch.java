package javalessons;

import java.util.Scanner;

public class TaskSwitch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				//System.out.println(); 			
				//Scanner scanner = new Scanner(System.in); //  leer en info de la consola		
				//System.out.println("Inserta un numero y presiona enter"); 
				//int a = scanner.nextInt();
		
		
		/*var numero  = 10;
		var numeroTexto = "numero desconocido";
		
		switch(numero) {
		case 1 :
			numeroTexto = "numero uno";
			break;
		
		case 2:
			numeroTexto = "numero dos";
			break;
			
		default:
			numeroTexto = "numero desconocido";
		}
		System.out.println("numero texto:"+ numeroTexto); 	
		*/
		Scanner scanner = new Scanner(System.in); //  leer en info de la consola		
		System.out.println("Inserta un numero y presiona enter"); 
		var numero = scanner.nextInt();
		
		String estacion = null;
		
		switch(numero) {
		case 1 : case 2: case 3:
			estacion = "invierno";
			break;
		
		case 4:case 5:case 6:
			estacion = "verano";
			break;
			
		default:
			estacion = "mes incorrecto";
		}
		System.out.println("la estacion es "+ estacion + " para el mes " + numero); 	
		
}
}
