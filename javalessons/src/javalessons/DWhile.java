package javalessons;

public class DWhile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//declarar variables en java reglas
		
		//System.out.println(); 
		//  \n NUEVA LINEA ESPACIADOR
		//    \' comilla simple
		//    \" comilla doble
		
		String nombre = "Beatriz ";
		var apellido = "Romero";
		
		System.out.println(nombre + apellido);
		
		System.out.println("espaciador \n" + nombre + apellido);
		
		System.out.println("tabulador \t" + nombre + apellido);
		System.out.println("retroceso \b" + nombre + apellido);
		
}
}
