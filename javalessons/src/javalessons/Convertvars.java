package javalessons;

import java.util.Scanner;

public class Convertvars {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//System.out.println(); 
		var edad = Integer.parseInt("20");// convierte string en int
		System.out.println(edad);
		char c = "hola".charAt(2);
		System.out.println(c); 
		
		
		// programa 2
		System.out.println("inserta la edad"); 
		
		var scanner = new Scanner(System.in);
		
		edad = Integer.parseInt(scanner.nextLine()); // toma la variable digitada en consola y la vuelve entero
		System.out.println("edad "+ edad); 
		
		System.out.println("inserta palabra"); 
		char caracter = scanner.nextLine().charAt(0);
		System.out.println("caracter= "+ caracter); 
		
		
		String edadTexto= String.valueOf(25);
		System.out.println(edadTexto); 
		
		
}
}
