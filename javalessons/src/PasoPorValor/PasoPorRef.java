package PasoPorValor;

public class PasoPorRef {

	String nombre;
	
	public String obtenerNombre() {
		
		return this.nombre;
	}
	
	public void cambiarNombre(String nuevoNombre) {
		this.nombre = nuevoNombre;
	}

}
