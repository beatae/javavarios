

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;




public class AutoSuggestion {

	public static void main(String[] args) throws InterruptedException  {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe" );
		WebDriver driver=new ChromeDriver();
		//Script to open a selector and add two adults and click on done
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
		driver.findElement(By.id("autosuggest")).sendKeys("ind");
		Thread.sleep(3000L);
		
		List<WebElement> options =driver.findElements(By.cssSelector("li[class='ui-menu-item'] a ")); // throws the list of options in one list
		
		for(WebElement option:options ) {
			if(option.getText().equalsIgnoreCase("India")) {
				option.click();
				break; // loops on the previous list and picks the exact one
			}
				
		}
		
		
	}

}
