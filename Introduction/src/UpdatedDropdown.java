import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class UpdatedDropdown {

	public static void main(String[] args) throws InterruptedException  {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe" );
		WebDriver driver=new ChromeDriver();
		//Script to open a selector and add two adults and click on done
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
		
		driver.findElement(By.id("divpaxinfo")).click();
		Thread.sleep(2000L);
		
		System.out.println(driver.findElement(By.id("divpaxinfo")).getText());
		 // initialize 
		int i=1;
		
		while(i<5) {
			//click up to 5 times, this is necessary so the script click only 4 times.
			driver.findElement(By.id("hrefIncAdt")).click();
			i++;
		}
		
		/*for(int i=1; i<5; i++) {
			driver.findElement(By.id("hrefIncAdt")).click();
		}*/
		
		
		driver.findElement(By.id("btnclosepaxoption")).click();
		System.out.println(driver.findElement(By.id("divpaxinfo")).getText()); //prints the field
		
	}

}
