

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;




public class greenkart {

	public static void main(String[] args) throws InterruptedException  {
		// TODO Auto-generated method stub

		String[] itemsNeeded = {"Cucumber", "Brocolli","Beetroot","Tomato"}; /// set the array to find for these products
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe" );
		WebDriver driver=new ChromeDriver();
				
		driver.get("https://rahulshettyacademy.com/seleniumPractise/");
		Thread.sleep(3000);
		addItems(driver,itemsNeeded);
		base b=new base();
		
		//gets the elements in a list or array
				
		driver.findElements(By.cssSelector("h4.product-name"));
		
		
		
	}



public static void addItems(WebDriver driver, String[] itemsNeeded) {

	int j=0;
	
	//
	
	
	List<WebElement>  products=driver.findElements(By.cssSelector("h4.product-name"));
	
	for(int i=0;i<products.size();i++) {
		
	
		// format the name to get actual vegetable name   Brocolli - 1 Kg	
	String[] name = products.get(i).getText().split("-");
	String formattedName= name[0].trim();

	
	
	//convert array into array list for easy search
	List itemsNeededList = Arrays.asList(itemsNeeded);
	
	//check whether name you extracted is present in array or not
	
	
	if(itemsNeededList.contains(formattedName)) {
		
		
		//apply click on
		driver.findElements(By.xpath("//div[@class='product-action']")).get(i).click();
		
		if(j==itemsNeeded.length) {
			break;
		 
		}
	
	
	}
	
}
	
}
}


