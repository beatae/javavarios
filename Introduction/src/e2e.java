import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;


public class e2e {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe" );
		WebDriver driver=new ChromeDriver();
		driver.get("https://www.spicejet.com/");
		
		//Seleccionar desde hasta y fecha current en el calendario
		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		
		Thread.sleep(2000L);

		driver.findElement(By.xpath("//div[@id='ctl00_mainContent_ddl_originStation1_CTNR'] //a[@value='MAA']")).click(); // this is the same but with parent child relationships
		driver.findElement(By.xpath("//div[@id='ctl00_mainContent_ddl_destinationStation1_CTNR'] //a[@value='BHO']")).click();
		
		driver.findElement(By.cssSelector(".ui-state-default.ui-state-highlight.ui-state-active")).click();/// select current date in the calendar with active class
		
		
		//selecciona los checkboxes
		
				
		Assert.assertFalse(driver.findElement(By.id("ctl00_mainContent_chk_SeniorCitizenDiscount")).isSelected());
		
		driver.findElement(By.id("ctl00_mainContent_chk_SeniorCitizenDiscount")).click(); // selects the box
		System.out.println(driver.findElement(By.id("ctl00_mainContent_chk_SeniorCitizenDiscount")).isSelected());// prints again
		Assert.assertTrue(driver.findElement(By.id("ctl00_mainContent_chk_SeniorCitizenDiscount")).isSelected());
		
		//System.out.println(driver.findElements(By.cssSelector("input[type='checkbox']")).size()); // prints the quantity of checkboxes in the page
		
		Thread.sleep(2000L);
		WebElement staticDropdown = driver.findElement(By.id("ctl00_mainContent_DropDownListCurrency"));
		Select dropdown =new Select(staticDropdown);
		dropdown.selectByIndex(3); //selects the fourth option of the index
		//System.out.println(dropdown.getFirstSelectedOption().getText());//prints the option selected in console
		dropdown.selectByVisibleText("AED");//selects by visible text in this case AED
		//System.out.println(dropdown.getFirstSelectedOption().getText());
		dropdown.selectByValue("INR");//selects by value
		System.out.println(dropdown.getFirstSelectedOption().getText());

		
	}

}
