



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
//TestNG testing framework 



public class calendarSelect {

	public static void main(String[] args) throws InterruptedException  {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe" );
		WebDriver driver=new ChromeDriver();
		//Script to open a selector and add two adults and click on done
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
		Assert.assertFalse(driver.findElement(By.id("ctl00_mainContent_chk_friendsandfamily")).isSelected());
		
		driver.findElement(By.id("ctl00_mainContent_chk_friendsandfamily")).click(); // selects the box
		System.out.println(driver.findElement(By.id("ctl00_mainContent_chk_friendsandfamily")).isSelected());// prints again
		Assert.assertTrue(driver.findElement(By.id("ctl00_mainContent_chk_friendsandfamily")).isSelected());
		
		System.out.println(driver.findElements(By.cssSelector("input[type='checkbox']")).size()); // prints the quantity of checkboxes in the page
		
		//checkboxes
		driver.findElement(By.id("divpaxinfo")).click();
		Thread.sleep(2000L);
		
		System.out.println(driver.findElement(By.id("divpaxinfo")).getText());
		
		for(int i=1; i<5; i++) {
			driver.findElement(By.id("hrefIncAdt")).click();
			
		}
		driver.findElement(By.id("btnclosepaxoption")).click();
		Assert.assertEquals(driver.findElement(By.id("divpaxinfo")).getText(), "5 Adult");/// compares the arguments
		System.out.println(driver.findElement(By.id("divpaxinfo")).getText()); //prints the field
	}

}
