import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class windowHandles {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe" );
		WebDriver driver=new ChromeDriver();
		driver.get("https://rahulshettyacademy.com/loginpagePractise/#");
		driver.findElement(By.cssSelector("a.blinkingText")).click();
		Set<String> windows = driver.getWindowHandles(); // metodo para encontrar las ventanas
		Iterator<String> it=windows.iterator();// itera sobre las ventanas y las mete en una lista
		String parentId = it.next(); /// primera ventana
		String childId = it.next(); // segunda ventana
		driver.switchTo().window(childId); //pasa a la segunda ventana
		System.out.println(driver.findElement(By.cssSelector(".im-para.red")).getText());//obtiene el letrero donde esta el mail
		
		String emailId = driver.findElement(By.cssSelector(".im-para.red")).getText().split("at")[1].trim().split(" ")[0];// se corta el letrero dos veces dos veces
		driver.switchTo().window(parentId); // se pasa a la primera ventana
		driver.findElement(By.id("username")).sendKeys(emailId); // se inserta el mail de la ventana 2
		
	}

}
