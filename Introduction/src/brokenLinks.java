import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class brokenLinks {

	public static void main(String[] args) throws MalformedURLException, IOException {
		// TODO Auto-generated method stub
//selenium code
		//create chrome driver object
		//invoke browser text
		System.setProperty("webdriver.chrome.driver","C:\\chromedriver\\chromedriver.exe" );
		WebDriver driver=new ChromeDriver();
		
		//broken url
		//Java methods that call urls and get the status codes
		//1)Get all the url tied up to the link
		//if status code is greater than 400 the url not working
		//a[href*='soaPUI']'
		
		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		
		
		List<WebElement> links = driver.findElements(By.cssSelector("li[class='gf-li'] a"));
		SoftAssert a = new SoftAssert();
		
		for (WebElement link : links) {
			 
			String url = link.getAttribute("href");
			 
			 HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
				conn.setRequestMethod("HEAD");
				conn.connect();
				int respCode = conn.getResponseCode();
				
				System.out.println(respCode);
				a.assertTrue(respCode<400, "The link with the text"+ link.getText()+" is broken with code " + respCode);
				
				
			
		}
		
		a.assertAll();
		
		
		
		
	}

}
