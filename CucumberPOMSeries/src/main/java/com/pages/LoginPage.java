package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	private WebDriver driver;
	
	
	//1. By locators
		
	private By emailId =  By.id("email");
	private By password = By.id("passwd");
	private By signButton = By.id("SubmitLogin");
	private By forgotPwd =  By.linkText("Forgot your password?");
	
	//2. Constructor of the page
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public String getLoginPageTitle() {
		return driver.getTitle();
	}
	
	public boolean isForgotPwd() {
		return driver.findElement(forgotPwd).isDisplayed();
		
		
	}
	
	public void enterUserName (String username) {
	driver.findElement(emailId).sendKeys(username);
	
	}
	
	public void enterPassword (String pwd) {
		driver.findElement(password).sendKeys(pwd);
	}
	
	public void clickonLogin() {
		driver.findElement(signButton).click();
	}
}
