package stepdefinitions;

import org.junit.Assert;

import com.factory.DriverFactory;
import com.pages.LoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {
	
	private static String pTitle;
	
	private LoginPage loginPage = new LoginPage(DriverFactory.getDriver());
	
	

	@Given("User is on login page")
	public void user_is_on_login_page() {
		DriverFactory.getDriver().get("http://automationpractice.com/index.php?controller=authentication");
	}

	@When("user gets the page title")
	public void user_gets_the_page_title() {
		String pTitle = loginPage.getLoginPageTitle();
	    System.out.println("login page title is "+ pTitle);
	}

	@Then("page title should be {string}")
	public void page_title_should_be(String expectedTitle) {
		String pTitle = loginPage.getLoginPageTitle();
	    System.out.println("login page title is "+ pTitle);
	    Assert.assertTrue(pTitle.contains(expectedTitle));
	}

	@Then("Forgot your password? should be displayed")
	public void forgot_your_password_should_be_displayed() {
	    Assert.assertTrue(loginPage.isForgotPwd());
	}

	@Given("user is on home login page")
	public void user_is_on_home_login_page() {
	  
	}

	@When("user enters username {string}")
	public void user_enters_username(String username) {
	   loginPage.enterUserName(username);
	}

	@When("user enter password {string}")
	public void user_enter_password(String password) {
	   loginPage.enterPassword(password);
	}

	@When("user clicks on login button")
	public void user_clicks_on_login_button() {
	   loginPage.clickonLogin();
	}

	

	
}
