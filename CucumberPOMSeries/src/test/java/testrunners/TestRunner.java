package testrunners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"C:\\Users\\romerob\\Documents\\javavarios\\CucumberPOMSeries\\src\\test\\sourcefolder\\com\\features\\login.feature"},
		glue = {"stepdefinitions", "AppHooks"},
		plugin = {"pretty"}
		
		)
public class TestRunner {

}
