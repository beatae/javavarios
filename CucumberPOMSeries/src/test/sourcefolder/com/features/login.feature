Feature: Login page feature

Scenario: Login page title
Given User is on login page
When user gets the page title
Then page title should be "Login - My Store"

Scenario: Forgot password link
Given User is on login page
Then Forgot your password? should be displayed

Scenario: Login with correct credentials
Given user is on home login page
When user enters username "dec2020secondbatch@gmail.com"
And user enter password "Selenium@12345"
And user clicks on login button
Then user gets the page title
And page title should be "My account - My Store"


